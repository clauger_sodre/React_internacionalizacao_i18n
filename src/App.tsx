import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { useTranslation } from "react-i18next";

function App() {
  const { t, i18n } = useTranslation();

  const changeLanguages = (language:any) => {
    i18n.changeLanguage(language);
  };
  console.log('carregou o app')
  return (
    <div className="App">
      <button onClick={() => changeLanguages("en")}>EN</button>
      <button onClick={() => changeLanguages("pt")}>PT</button>
      <hr />
      <div><h1>{t("title")}</h1></div>
      <div>{t("description.part1")}</div>
      <div>{t("description.part2")}</div>
      <h1>ola</h1>
      {console.log(t("description.part2"))}
    </div>
  );
}

export default App;
